# Umbrella Application

### Frameworks used:

 * Backend: Ruby on Rails
 * Database: MySQL
 * Frontend: AngularJS (angular-rails-templates)
 * Template: Twitter Bootstrap 3+
 * Dependencies: bower-rails

### Get Started

```
sudo apt-get install bundler
sudo apt-get install rake
rake bower:install
```

### Run server

```ruby
rails server
# OR
rails s (alias)
```

### Issue tracker