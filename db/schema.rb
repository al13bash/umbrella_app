# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160215100357) do

  create_table "credit_cards", force: :cascade do |t|
    t.string   "full_name",       limit: 255
    t.string   "address_line_1",  limit: 255
    t.string   "address_line_2",  limit: 255
    t.string   "country",         limit: 255
    t.string   "city",            limit: 255
    t.string   "state",           limit: 255
    t.string   "postal",          limit: 255
    t.string   "card_number",     limit: 255
    t.string   "month",           limit: 255
    t.string   "year",            limit: 255
    t.string   "cvv",             limit: 255
    t.integer  "subscription_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "credit_cards", ["subscription_id"], name: "index_credit_cards_on_subscription_id", using: :btree

  create_table "galleries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "position",   limit: 4
    t.integer  "project_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "galleries", ["project_id"], name: "index_galleries_on_project_id", using: :btree

  create_table "options", force: :cascade do |t|
    t.string   "custom_domain",              limit: 255
    t.string   "homepage_password",          limit: 255
    t.boolean  "display_contact_info",       limit: 1,   default: true
    t.boolean  "social_media_functionality", limit: 1,   default: true
    t.integer  "user_id",                    limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "watermark",                  limit: 255
  end

  add_index "options", ["user_id"], name: "index_options_on_user_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.string   "photo",      limit: 255
    t.integer  "gallery_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "size",       limit: 8
  end

  add_index "photos", ["gallery_id"], name: "index_photos_on_gallery_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name",                    limit: 255
    t.date     "date"
    t.string   "tags",                    limit: 255
    t.date     "expiry"
    t.boolean  "registration",            limit: 1,   default: false
    t.boolean  "homepage",                limit: 1,   default: true
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.integer  "user_id",                 limit: 4
    t.string   "project_url",             limit: 255
    t.string   "password",                limit: 255
    t.string   "download_pin",            limit: 255
    t.boolean  "watermark",               limit: 1
    t.boolean  "allow_origin_photo_size", limit: 1
    t.date     "expiration_date"
    t.boolean  "favourites",              limit: 1
    t.boolean  "sharing",                 limit: 1
    t.boolean  "store",                   limit: 1
  end

  add_index "projects", ["user_id"], name: "index_projects_on_user_id", using: :btree

  create_table "subscription_plans", force: :cascade do |t|
    t.integer  "members",         limit: 4
    t.string   "products",        limit: 255
    t.string   "file_uploads",    limit: 255
    t.string   "billing",         limit: 255
    t.string   "name",            limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "premium_support", limit: 255
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string   "storage",              limit: 255
    t.integer  "user_id",              limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "subscription_plan_id", limit: 4
  end

  add_index "subscriptions", ["user_id"], name: "index_subscriptions_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "provider",               limit: 255,   default: "email", null: false
    t.string   "uid",                    limit: 255,   default: "",      null: false
    t.string   "encrypted_password",     limit: 255,   default: "",      null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "name",                   limit: 255
    t.string   "studio_name",            limit: 255
    t.string   "email",                  limit: 255
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "studio_logo",            limit: 255
    t.string   "contact_person",         limit: 255
    t.string   "address",                limit: 255
    t.string   "phone",                  limit: 255
    t.string   "website",                limit: 255
    t.string   "facebook",               limit: 255
    t.string   "twitter",                limit: 255
    t.string   "google",                 limit: 255
    t.string   "instagram",              limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  add_foreign_key "credit_cards", "subscriptions", on_delete: :cascade
  add_foreign_key "galleries", "projects", on_delete: :cascade
  add_foreign_key "options", "users", on_delete: :cascade
  add_foreign_key "photos", "galleries", on_delete: :cascade
  add_foreign_key "projects", "users", on_delete: :cascade
  add_foreign_key "subscriptions", "users", on_delete: :cascade
end
