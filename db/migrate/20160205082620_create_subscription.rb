class CreateSubscription < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :plan
      t.string :credit_card
      t.string :storage
      t.belongs_to :user, index: true

      t.timestamps null: false
    end

    add_foreign_key :subscriptions, :users, on_delete: :cascade
  end
end
