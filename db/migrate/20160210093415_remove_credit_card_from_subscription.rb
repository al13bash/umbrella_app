class RemoveCreditCardFromSubscription < ActiveRecord::Migration
  def change
    remove_column :subscriptions, :credit_card
  end
end
