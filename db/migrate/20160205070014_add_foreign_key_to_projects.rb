class AddForeignKeyToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :user_id, :integer
    add_foreign_key :projects, :users, on_delete: :cascade

    add_index :projects, :user_id
  end
end
