class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.date :date
      t.string :tags
      t.date :expiry
      t.boolean :registration, default: false
      t.boolean :homepage, default: true

      t.timestamps null: false
    end
  end
end
