class RemoveColumnsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :nickname
    remove_column :users, :image
  end
end
