class AddSubscriptionPlanIdToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :subscription_plan_id, :integer, limit: 4
  end
end
