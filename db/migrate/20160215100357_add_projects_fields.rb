class AddProjectsFields < ActiveRecord::Migration
  def change
    add_column :projects, :project_url, :string
    add_column :projects, :password, :string
    add_column :projects, :download_pin, :string
    add_column :projects, :watermark, :boolean, default: false
    add_column :projects, :allow_origin_photo_size, :boolean, default: false
    add_column :projects, :expiration_date, :date
    add_column :projects, :favourites, :boolean, default: false
    add_column :projects, :sharing, :boolean, default: false
    add_column :projects, :store, :boolean, default: false
  end
end
