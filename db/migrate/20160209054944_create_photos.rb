class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :photo
      t.belongs_to :gallery, index: true

      t.timestamps null: false
    end

    add_foreign_key :photos, :galleries, on_delete: :cascade
  end
end
