class AddUserFields < ActiveRecord::Migration
  def change
    add_column :users, :studio_logo, :string
    add_column :users, :contact_person, :string
    add_column :users, :address, :string
    add_column :users, :phone_number, :string
    add_column :users, :website, :string
    add_column :users, :facebook, :string
    add_column :users, :twitter, :string
    add_column :users, :google, :string
    add_column :users, :instagram, :string
  end
end
