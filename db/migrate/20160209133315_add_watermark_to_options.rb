class AddWatermarkToOptions < ActiveRecord::Migration
  def change
    add_column :options, :watermark, :string
  end
end
