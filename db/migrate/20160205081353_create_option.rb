class CreateOption < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.string :custom_domain
      t.string :homepage_password
      t.boolean :display_contact_info, default: true
      t.boolean :social_media_functionality, default: true
      t.belongs_to :user, index: true

      t.timestamps null: false
    end

    add_foreign_key :options, :users, on_delete: :cascade
  end
end
