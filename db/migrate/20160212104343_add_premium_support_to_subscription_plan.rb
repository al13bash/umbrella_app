class AddPremiumSupportToSubscriptionPlan < ActiveRecord::Migration
  def change
    add_column :subscription_plans, :premium_support, :string
  end
end
