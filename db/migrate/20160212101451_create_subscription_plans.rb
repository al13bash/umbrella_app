class CreateSubscriptionPlans < ActiveRecord::Migration
  def change
    create_table :subscription_plans do |t|
      t.integer :members, default: 0
      t.string :products
      t.string :file_uploads
      t.string :billing
      t.string :name

      t.timestamps null: false
    end
  end
end
