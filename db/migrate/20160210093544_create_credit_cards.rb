class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string :full_name
      t.string :address_line_1
      t.string :address_line_2
      t.string :country
      t.string :city
      t.string :state
      t.string :postal
      t.string :card_number
      t.string :month
      t.string :year
      t.string :cvv
      t.belongs_to :subscription, index: true

      t.timestamps null: false
    end

    add_foreign_key :credit_cards, :subscriptions, on_delete: :cascade
  end
end
