class AddSizeToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :size, :integer, limit: 8, default: 0
  end
end
