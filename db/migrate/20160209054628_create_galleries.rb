class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :name
      t.integer :position
      t.belongs_to :project, index: true

      t.timestamps null: false
    end

    add_foreign_key :galleries, :projects, on_delete: :cascade
  end
end
