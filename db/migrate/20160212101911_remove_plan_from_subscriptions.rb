class RemovePlanFromSubscriptions < ActiveRecord::Migration
  def change
    remove_column :subscriptions, :plan
  end
end
