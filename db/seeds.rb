# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

SubscriptionPlan.create(members: 1, products: 'unlimited', file_uploads: 'unlimited',
                        billing: 'free', name: 'Basic')
SubscriptionPlan.create(members: 5, products: 'unlimited', file_uploads: 'unlimited',
                        billing: '$19/month billed annually', name: 'Startup')
SubscriptionPlan.create(members: 15, products: 'unlimited', file_uploads: 'unlimited',
                        billing: '$59/month billed annually', name: 'Company')
SubscriptionPlan.create(members: 25, products: 'unlimited', file_uploads: 'unlimited',
                        billing: '$149/month billed annually', name: 'Enterprise', premium_support: '24-Hour')
