FactoryGirl.define do
  factory :project do
    name 'MyString'
    date '2016-02-02'
    tags 'MyString'
    expiry '2016-02-02'
    registration false
    homepage false
  end
end
