json.extract! @option, :id, :custom_domain, :homepage_password, :display_contact_info, :social_media_functionality
json.watermark @option.watermark.url
