json.extract! @user, :id, :name, :studio_name, :email, :contact_person, :address,
              :phone, :website, :facebook, :twitter, :google, :instagram
json.studio_logo @user.studio_logo.url
