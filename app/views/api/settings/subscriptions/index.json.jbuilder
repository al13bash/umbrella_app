json.extract! @subscription, :id, :subscription_plan_id
json.storage number_with_precision(@subscription.storage.to_f / 1_000_000_000, precision: 2)

credit_card = @subscription.credit_card ? @subscription.credit_card : CreditCard.create
json.credit_card credit_card, :full_name, :address_line_1, :address_line_2, :country,
                 :city, :state, :postal, :card_number, :month, :year, :cvv

json.subscription_plan @subscription.subscription_plan, :members, :products,
                       :file_uploads, :billing, :name, :premium_support
