json.array!(@photos) do |photo|
  json.extract! photo, :id
  json.url photo.url
end
