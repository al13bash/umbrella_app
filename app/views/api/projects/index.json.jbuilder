json.projects @projects do |project|
  json.extract! project, :id, :name, :date, :expiry, :registration, :homepage
  json.tags project.tags.split(', ')
end
