json.success true
json.data do
  json.extract! @project,
                :watermark, :project_url, :expiration_date,
                :sharing, :favourites, :store, :allow_origin_photo_size,
                :password, :download_pin
end