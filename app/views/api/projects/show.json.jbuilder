json.extract! @project, :id, :name, :date, :expiry, :registration, :homepage
json.tags @project.tags.split(', ')
json.galleries @project.galleries, partial: 'api/galleries/item', as: :gallery
