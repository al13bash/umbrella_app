(function() {
    'use strict';

    angular
        .module('umbrella.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$routeProvider', '$locationProvider'];
    function routeConfig($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl : "partials/auth/sign_in.html",
                controller  : 'SignInController',
                controllerAs: 'signin'
            })
            .when('/sign_in', {
                templateUrl : "partials/auth/sign_in.html",
                controller  : 'SignInController',
                controllerAs: 'signin'
            })
            .when('/sign_up', {
                templateUrl : "partials/auth/sign_up.html",
                controller  : 'SignUpController',
                controllerAs: 'signup'
            })
            .when('/passwords/new', {
                templateUrl : "partials/auth/reset_password_form.html",
                controller  : 'ResetPasswordController',
                controllerAs: 'resetpwd'
            })
            .when('/reset_password', {
                templateUrl : "partials/auth/reset_password.html",
                controller  : 'ResetPasswordController',
                controllerAs: 'resetpwd'
            })
            .when('/account', {
                templateUrl : "partials/account/settings.html",
                controller  : 'AccountController',
                controllerAs: 'account'
            })
            .when('/account/edit', {
                templateUrl : "partials/account/edit.html",
                controller  : 'AccountController',
                controllerAs: 'account'
            })
            .when('/account/subscription', {
                templateUrl : "partials/account/subscription.html",
                controller  : 'AccountController',
                controllerAs: 'account'
            })
            .when('/account/subscription/edit_credit_card', {
                templateUrl : "partials/account/edit_credit_card.html",
                controller  : 'AccountController',
                controllerAs: 'account'
            })
            .when('/account/options', {
                templateUrl : "partials/account/options.html",
                controller  : 'AccountController',
                controllerAs: 'account'
            })
            .when('/projects', {
                templateUrl : "partials/projects/dashboard.html",
                controller  : 'ProjectsController',
                controllerAs: 'projects'
            })
            .when('/projects/project/:project_id', {
                templateUrl : "partials/projects/project.html",
                controller  : 'ProjectController',
                controllerAs: 'project'
            })
            .when('/store', {
                templateUrl : "partials/store/my_orders.html",
                controller  : 'StoreController',
                controllerAs: 'store'
            })
            .otherwise({
                redirectTo: '/'
            });
        $locationProvider.html5Mode(true).hashPrefix('!');
    }
})();