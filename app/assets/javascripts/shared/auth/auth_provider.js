(function () {
    'use strict';
    angular.module('umbrella.auth', ['ng-token-auth'])
        .config(function($authProvider) {
            $authProvider.configure({
                apiUrl: window.location.origin,
                passwordResetSuccessUrl: window.location.origin + "/passwords/new"
            });
        });
})();