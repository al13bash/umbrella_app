(function () {
    'use strict';
    angular
        .module('umbrella.directives')
        .directive('breadCrumbs', breadCrumbs);

    function breadCrumbs() {
        var directive = {
            restrict : 'EA',
            replace: true,
            templateUrl: 'shared/breadcrumbs/breadcrumbs.html',
            link: function(scope, element, attrs) {}
        };
        return directive;
    }
})();