(function() {
    'use strict';
    angular
        .module('umbrella.resource')
        .factory('APIResource', APIResource)
        .factory('SubscriptionResource', SubscriptionResource)
        .factory('CreditCardResource', CreditCardResource)
        .factory('OptionsResource', OptionsResource);

    var hostPlaceholder = "http://localhost:3000";


    var HOST = "http://localhost:3000";

    APIResource.$inject = ['$resource', 'Upload'];

    function APIResource($resource, Upload) {

        return {
            CreditCard: $resource(HOST + '/api/credit_card', {
                id: '@id'
            }, {
                query: {
                    isArray: false
                }
            }),
            Options: (function() {
                var apiPath = HOST + '/api/options'

                var resource = $resource(apiPath, {
                    id: '@id'
                }, {
                    query: {
                        isArray: false
                    }
                });
                resource.uploadWatermark = function(file) {
                    return Upload.upload({
                        url: apiPath + '/watermark',
                        data: {watermark: file}
                    });
                }
                return resource;
            })()
            ,
            Account: (function() {
                var apiPath = HOST + '/api/account'
                var resource = $resource(apiPath);
                //Custom Resource methods
                resource.uploadLogo = function(file) {
                    return Upload.upload({
                        url: apiPath + '/logo',
                        data: { studio_logo: file }
                    });
                }
                return resource;
            })(),
            Projects: (function() {
                var apiPath = HOST + '/api/projects/:project_id',
                    resource = $resource(apiPath, {
                        project_id: '@id'
                    }, {
                        query: {
                            isArray: false
                        }
                    });
                resource.Options = $resource(apiPath + '/options');
                resource.Galleries = (function() {
                    var apiGallery = apiPath + '/galleries/:gallery_id',
                        resource = $resource(apiGallery, {
                            gallery_id: '@id',
                            project_id: '@project_id'
                        });
                    resource.Photos = (function() {
                        var api_photos = apiGallery + '/photos/:photo_id';
                        var resource = $resource(api_photos, {
                            photo_id: '@id',
                            gallery_id: '@gallery_id'
                        });
                        resource.uploadImages = function(projectID, galleryID, file) {
                            var url = HOST + '/api/projects/' + projectID + '/galleries/' + galleryID + '/photos';
                            return Upload.upload({
                                url: url,
                                data: {
                                    photo: file
                                }
                            }).then(function(response) {}, function(response) {
                                console.log('Error status: ' + response.status);
                            }, function(evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                var progressBar = progressPercentage + '% ';
                            });
                        }
                        return resource;
                    })()
                    return resource;
                })();
                return resource;
            })(),
            Subscription: $resource(HOST + '/api/subscription', {
                id: '@id'
            }, {
                query: {
                    isArray: false
                }
            })
        };
    }

    var APIS = {
        options: hostPlaceholder + '/api/options',
        subscription: hostPlaceholder + '/api/subscription',
        credit_card: hostPlaceholder + '/api/credit_card'
    };

    SubscriptionResource.$inject = ['$resource'];

    function SubscriptionResource($resource) {
        return $resource(APIS.subscription);
    }

    OptionsResource.$inject = ['$resource'];

    function OptionsResource($resource) {
        return $resource(APIS.options);
    }

    CreditCardResource.$inject = ['$resource'];

    function CreditCardResource($resource) {
        return $resource(APIS.credit_card);
    }

})();
