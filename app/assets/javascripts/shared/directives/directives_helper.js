(function () {
    'use strict';
    angular
        .module('umbrella.directives')
        .directive('fitHeight', fitHeight)
        .directive('storage', storage);

    function fitHeight() {
        var directive = {
            restrict : 'EA',
            link: function(scope, element, attrs) {

                fitheightCallback();
                window.addEventListener('resize', fitheightCallback);
                
                function fitheightCallback(){
                    var     windowHeight = document.documentElement.clientHeight;
                    var     navbarHeight = document.querySelector('.navbar').clientHeight;
                    var breadcrumbHeight = document.querySelector('.breadcrumb').clientHeight;
                    var              sum = windowHeight - (navbarHeight + breadcrumbHeight);
                    element.css('min-height', sum + 'px');
                }
            }
        };
        return directive;
    }


    function storage() {
        var directive = {
            restrict : 'EA',
            replace  : true,
            scope: {
                space : '@'
            },
            template : '<strong>{{space}}GB</strong>',
            link: function(scope, element, attrs) {}
        };
        return directive;
    }
})();
