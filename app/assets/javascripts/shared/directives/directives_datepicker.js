(function() {
    'use strict';
    angular
        .module('umbrella.directives')
        .directive('datePicker', datePicker);

    function datePicker() {
        var directive = {
            restrict: 'EA',
            require: "ngModel",
            link: function(scope, elem, attrs, ngModelCtrl) {
                var updateModel = function(dateText) {
                    scope.$apply(function() {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "yy-mm-dd",
                    onSelect: function(dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        };
        return directive;
    }
})();
