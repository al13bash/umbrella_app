(function() {
    'use strict';
    angular
        .module('umbrella.directives')
        .directive('navBar', navBar);

    /*
     * Header/navBar Directive
     * -----------
     * @usage     <nav-bar help-dropdown="false"></nav-bar>
     * 
     * @restrict  'E' = element, 'A' = attribute
     * @scope     helpDropdown | Boolean | Help links dropdown
     * @template  /public/shared/header.html
     *
     * @author    @_elmahdim
     */
    function navBar() {
        var directive = {
            restrict: 'EA',
            replace: true,
            templateUrl: 'shared/header.html',
            scope: {
                helpDropdown: '@'
            },
            link: function(scope, element, attrs) {}
        };
        return directive;
    }
})();
