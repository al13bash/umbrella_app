(function () {
    'use strict';
    angular
        .module('umbrella.controllers')
        .controller('ResetPasswordController', SignInController);

    SignInController.$inject = ['$scope', '$http', '$location', '$auth', '$rootScope'];
    function SignInController($scope, $http, $location, $auth, $rootScope) {

        $scope.handleResetBtnClick = function() {
            $auth.updatePassword($scope.updatePasswordForm);
        };

        $scope.handleLoginBtnClick = function() {
            $auth.submitLogin($scope.resetForm)
                .then(function(resp) {

                });
        };

        $scope.$on('auth:password-change-success', function(ev, data) {
            $location.path("/account");
        });

        $scope.$on('auth:password-reset-request-success', function(ev, data) {
            alert("Password reset instructions were sent to " + data.email);
        });
    }
})();