(function () {
    'use strict';
    angular
        .module('umbrella.controllers')
        .controller('SignInController', SignInController);

    SignInController.$inject = ['$scope', '$http', '$location', '$auth', '$rootScope', '$timeout'];
    function SignInController($scope, $http, $location, $auth, $rootScope, $timeout) {
        $scope.dirtyError = false;

        $scope.handleLoginBtnClick = function() {
            $auth.submitLogin($scope.loginForm)
                .then(function(resp) {
                    
                });
        };

        $rootScope.$on('auth:login-error', function(ev, reason) {
            if ($scope.errors) {
             $scope.dirtyError = true;
             $timeout(function() {
                $scope.dirtyError = false;
             }, 1500);
            }
            $scope.errors = reason.errors[0];
        });

        $scope.$on('auth:login-success', function(ev, user) {
            //alert('Welcome ', user.email);
            //$rootScope.currentUser = user;
            $location.path('/account');
        });
    }
})();