(function () {
    'use strict';
    angular
        .module('umbrella.controllers')
        .controller('SignUpController', SignUpController);

    SignUpController.$inject = ['$scope', '$http', '$auth', '$location', '$timeout'];
    function SignUpController($scope, $http, $auth, $location, $timeout) {
        $scope.$on('auth:registration-email-success', function(ev, message) {
            //alert("registration" + message.email);
            $location.path('/account');
        });

        $scope.$on('auth:registration-email-error', function(ev, reason) {
            if ($scope.errors) {
             $scope.dirtyError = true;
             $timeout(function() {
                $scope.dirtyError = false;
             }, 1500);
            }
            $scope.errors = reason.errors.full_messages;
        });


        $scope.handleRegBtnClick = function () {
            $auth.submitRegistration($scope.registrationForm)
                .then(function (resp) {

                })
                .catch(function (resp) {

                });
        };
    }
})();