(function() {
    'use strict';
    angular
        .module('umbrella.controllers')
        .controller('ProjectController', ProjectController);

    ProjectController.$inject = ['$scope', '$location', '$routeParams', 'APIResource', 'Upload', '$timeout'];

    function ProjectController($scope, $location, $routeParams, APIResource, Upload, $timeout) {
        $scope.hasLimit = true;
        $scope.flag     = false;
        $scope.coverLayout = false;
        $scope.photoLayout = false;

        $scope.field = {
          title: ""
        };

        initialize();
        function initialize() {
            $scope.tabs = [];
            $scope.project = APIResource.Projects.query({
                    project_id: $routeParams.project_id
                }).$promise
                .then(function(response) {
                    $scope.project = response;

                    $scope.project.galleries.forEach(function(item, value) {
                        $scope.tabs.push({title: item.name, content: 'tab-highlights.html'});
                    });
                    APIResource.Projects.Options.get({project_id: $scope.project.id}).$promise
                        .then(function(response){
                            $scope.options = response.data;
                        });
                }, function(response) {
                    console.error(response);
                });

            APIResource.Projects.Galleries.Photos.query({gallery_id: 8, project_id: $routeParams.project_id})
                .$promise.then(function(response){
                  console.log(response);
                  $scope.gFiles = response;
                });
        }
        
        $scope.$watch('gFiles', function() {
          $scope.upload($scope.gFiles);
        });
        $scope.upload = function(gFiles) {
          if (gFiles && gFiles.length) {
            for (var i = 0; i < gFiles.length; i++) {
              var file = gFiles[i];
              if (!file.$error) {
                APIResource.Projects.Galleries.Photos.uploadImages($scope.project.id, $scope.project.galleries[0].id, file);
              }
            }
          }
        }

        $scope.saveOptions = function(options) {
            APIResource.Projects.Options.save({project_id: $scope.project.id}, {project: options})
                .$promise
                .then();
        };
        $scope.deleteProject = function(project) {
          APIResource.Projects.delete({project_id: project.id}).$promise
              .then(function(response){
                  $location.path('/projects');
              });
        };
        $scope.createGallery = function(title, position) {
            APIResource.Projects.Galleries.save({project_id: $scope.project.id},
                {gallery: {name: title, position: position}})
                .$promise.then(function(response){

                });
        };
        $scope.deleteGallery = function(gallery){
            APIResource.Projects.Galleries.delete(gallery).$promise
                .then(function(response){

                });
        };
        $scope.changeLayout = function(layout){
          if(layout === 'cover') {
            $scope.coverLayout = true;
            $scope.photoLayout = false;
          } else {
            $scope.photoLayout = true;
            $scope.coverLayout = false;
          }
        };

        $scope.CreateNewTab = function() {
            if ($scope.field.title === "") {
                $scope.flag = true;
                $scope.message = "Required field";
            } else {
                $scope.flag = false;
                angular.forEach($scope.tabs, function(value, key) {
                  if ($scope.field.title == value.title) {
                    $scope.flag = true;
                  }
                  $(".nav-tabs").sortable({
                    axis: "x",
                    items: "> li:not(:last-child)",
                    helper: 'clone',
                    containment: ".nav-tabs",
                    placeholder: "ui-sortable-placeholder",
                    start: function(event, ui) {
                      $(".ui-sortable-placeholder").width(ui.item.width());
                      ui.item.parent().addClass("is-sorting");
                    },
                    stop: function(event, ui) {
                      $(".ui-sortable-placeholder").width(0);
                      ui.item.parent().removeClass("is-sorting");
                    },
                    update: function(event, ui) {
                      var uiArray = $(this).sortable("toArray");
                    }
                  });
                  $(".nav-tabs").disableSelection();
                });
                if (!$scope.flag) {
                  if ( $scope.tabs.length == 4 ){
                    $scope.hasLimit = false;
                  }
                  $scope.tabs.push({
                    title: $scope.field.title,
                    content: 'tab-placeholder.html',
                    active: true
                  });
                  $('#CreateNewTabModal').modal('hide');
                } else {
                  $scope.message = '"' + $scope.field.title + '" tab already exists!';
                }
                $scope.field.title = "";
            }
        }

    }
})();
