(function() {
    'use strict';
    angular
        .module('umbrella.controllers')
        .controller('ProjectsController', ProjectsController);

    ProjectsController.$inject = ['$scope', '$location', 'APIResource'];

    function ProjectsController($scope, $location, APIResource) {

        $scope.project = {
            name: "",
            date: "",
            tags: "",
            expiry: "",
            registration: false,
            homepage: true
        };


        initialize();

        function initialize() {
            $scope.projects = APIResource.Projects.query().$promise
                .then(function(response) {
                    $scope.projects = response.projects;
                }, function(response) {
                    console.error(response);
                });
        }

        $scope.addNewProject = function() {
            var tagsArray = new Array();
            var trimTags = $scope.project.tags.replace(/(^,*)|\,,/g, "");
            tagsArray = trimTags.split(",");
            $scope.project.tags = tagsArray;

            $scope.project = APIResource.Projects.save($scope.project).$promise
                .then(function(response) {
                    $location.path("/projects/project/" + response.id);
                }, function(response) {
                    console.error(response.statusText);
                });
        }
    }
})();
