(function () {
    'use strict';
    angular
        .module('umbrella.controllers')
        .controller('AccountController', AccountController);

    AccountController.$inject = ['$scope', '$http', '$location', '$auth', '$rootScope', 'APIResource', '$timeout'];
    function AccountController($scope, $http, $location, $auth, $rootScope, APIResource, $timeout) {

        initialize();

        function initialize() {
          $rootScope.currentUser = $rootScope.user;
          if($rootScope.subscription === undefined)
            $rootScope.subscription = APIResource.Subscription.get();
          if($rootScope.options === undefined)
            $rootScope.options = APIResource.Options.get();
        }

        $scope.activeHandler = function (viewLocation) {
            var active = (viewLocation === $location.path());
            return active;
        };

        $scope.changeSubscription = function() {
          APIResource.Subscription.save($scope.subscription);
        };

        var timeout;
        $scope.changeOption = function() {
            $timeout.cancel(timeout);
            timeout = $timeout(function() {
                APIResource.Options.save($scope.options);  
            }, 5000);
        };


        $scope.editCreditCard = function() {
          APIResource.CreditCard.save($rootScope.subscription.credit_card);
        };

        $scope.handleUpdateAccountBtnClick = function() {
          $auth.updateAccount($scope.user)
            .then(function(resp)  {})
            .catch(function(resp) {});
        };


        $scope.uploadLogo = function() {
          var file = $scope.file;
          APIResource.Account.uploadLogo(file)
            .then(function(response) {
              $rootScope.currentUser.studio_logo.url = response.data.url;
            })
            .catch(function(response) {
              console.error(response);
            });
        };

        $scope.uploadWatermark = function() {
          var file = $scope.file;
          APIResource.Options.uploadWatermark(file)
            .then(function(response) {
              $rootScope.options.watermark = response.data.url;
            })
            .catch(function(response) {
              console.error(response);
            });
        };

        $scope.$on('auth:account-update-success', function(ev) {
          $location.path('/account');
          initialize();
        });

        $scope.$on('auth:account-update-error', function(ev, reason) {
          console.log(reason.errors[0]);
        });
    }
})();