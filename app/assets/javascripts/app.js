(function() {
    'use strict';
    angular.module('umbrella.controllers', []);
    angular.module('umbrella.services', ['ngFileUpload']);
    angular.module('umbrella.resource', []);
    angular.module('umbrella.routes', []);
    angular.module('umbrella.directives', ['ui.bootstrap']);
    angular.module('umbrella.templates', []);

    var dependencies = [
        'ngRoute',
        'ngResource',
        'umbrella.controllers',
        'umbrella.auth',
        'umbrella.directives',
        'umbrella.routes',
        'umbrella.services',
        'umbrella.templates',
        'umbrella.resource'
    ];
    angular.module('umbrella', dependencies);
})();