(function($) {
    $(document).ready(function() {

        if ($('[fit-height]').length) {
            fitheightCallback();
            window.addEventListener('resize', fitheightCallback);
        }

        function fitheightCallback() {
            var navbarHeight = document.querySelector('.navbar').clientHeight;
            var breadcrumbHeight = document.querySelector('.breadcrumb').clientHeight;
            var sum = document.documentElement.clientHeight - (navbarHeight + breadcrumbHeight);

            $('[fit-height]').css('min-height', sum);
        }

        $(this).on('click', '[dataToggle]', function() {
            var attr = $(this).attr('dataToggle');
            $(attr).toggleClass('hidden');
        });

    });
})(jQuery);
