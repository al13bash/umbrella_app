# encoding: utf-8

class PhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def watermark!(logo_file)
    logo_image = Magick::Image.read(logo_file.path).first
    manipulate! do |img|
      img.composite!(logo_image, Magick::NorthWestGravity, 15, 0, Magick::OverCompositeOp)
    end
  end


end
