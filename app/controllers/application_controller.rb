class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_action :configure_devise_permitted_parameters, if: :devise_controller?

  def configure_devise_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :studio_name
    devise_parameter_sanitizer.for(:account_update) << [:studio_name, :studio_logo, :name,
                                                        :facebook, :twitter, :google,
                                                        :instagram, :contact_person,
                                                        :address, :phone, :website]
  end
end
