module Api
  module Settings
    class SubscriptionsController < BaseSettingsController
      before_action :set_subscription
      before_action :init_storage_details, only: :index

      def index
        @subscription
      end

      def update
        @subscription.update!(subscription_params)
        return render_success(@subscription)
      rescue => e
        return render_failure(e.message.gsub(/\A.+: /, '').split(', '))
      end

      private

      def subscription_params
        params.permit(:id, :storage, :subscription_plan_id)
      end

      def set_subscription
        @subscription = current_user.subscription
      end

      def init_storage_details
        @subscription.update!(storage: Subscription.calculate_storage_details(current_user))
      end
    end
  end
end
