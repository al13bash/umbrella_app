module Api
  module Settings
    class OptionsController < BaseSettingsController
      before_action :set_option

      def index
        @option
      end

      def update
        @option.update!(option_params)
        return render_success(@option)
      rescue => e
        return render_failure(e.message.gsub(/\A.+: /, '').split(', '))
      end

      def set_watermark
        render @option.update(watermark_params) ? :show : :error
      end

      def watermark
        render :show
      end

      private

      def set_option
        @option = current_user.option
      end

      def option_params
        params.permit(:id, :custom_domain, :homepage_password, :display_contact_info,
                      :social_media_functionality)
      end

      def watermark_params
        params.permit(:watermark).to_hash
      end
    end
  end
end
