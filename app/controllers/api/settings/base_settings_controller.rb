module Api
  module Settings
    class BaseSettingsController < ApplicationController
      def render_success(object)
        @object = object
        render :render_success
      end

      def render_failure(errors)
        @errors = errors
        render :render_failure
      end
    end
  end
end
