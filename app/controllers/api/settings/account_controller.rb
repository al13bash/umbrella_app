module Api
  module Settings
    class AccountController < BaseSettingsController
      before_action :authenticate_user!, :set_user
      def index
        @user
      end

      def set_logo
        render @user.update(logo_params) ? :show : :error
      end

      def logo
        render :show
      end

      private

      def set_user
        @user = current_user
      end

      def logo_params
        params.permit(:studio_logo).to_hash
      end
    end
  end
end
