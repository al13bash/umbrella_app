module Api
  module Settings
    class CreditCardController < BaseSettingsController
      before_action :set_credit_card, except: :update

      def index
        @credit_card
      end

      def update
        @credit_card = CreditCard.find_or_initialize_by(subscription_id: current_user.subscription.id)
        @credit_card.update!(credit_card_params)
        return render_success(@credit_card)
      rescue => e
        return render_failure(e.message.gsub(/\A.+: /, '').split(', '))
      end

      private

      def set_credit_card
        @credit_card = current_user.subscription.credit_card
      end

      def credit_card_params
        params.permit(:full_name, :address_line_1, :address_line_2, :country,
                      :city, :state, :postal, :card_number, :month, :year, :cvv)
      end
    end
  end
end
