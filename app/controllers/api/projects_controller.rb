module Api
  class ProjectsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_project, only: [:show, :set_options, :update, :options]


    def index
      @projects = current_user.projects
    end

    def show
      @project = Project.find_by_id(params[:id])
    end

    def create
      @project = current_user.projects.new(project_params)

       if @project.save
         render :show
       else
         render :errors
       end
    end

    def update
      render @project.update(project_params) ? :show : :errors
    end

    def set_options
      render @project.update(options_params) ? :options : :errors
    end

    def options
      render :options
    end

    private


    def set_project
      @project = Project.find(params[:id])
    end

    def options_params
      params.require(:project).permit(:watermark, :password, :allow_origin_photo_size, :project_url)
    end

    def project_params
      result = params.require(:project).permit(:name, :date, :expiry, :registration, :homepage, tags: [])
      result[:tags] = result[:tags].join(', ')
      result
    end
  end
end