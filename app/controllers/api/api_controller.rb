module Api
  class ApiController < ApplicationController
    include ::Pundit

    #after_action :verify_authorized
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    private

    def user_not_authorized
      render json: 'Not Authorized', status: 401
    end
  end
end