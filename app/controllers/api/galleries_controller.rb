module Api
  class GalleriesController < ApiController
    before_action :set_gallery, only: [:show, :update, :destroy]
    before_action :set_project


    def index
      @galleries = @project.galleries
    end

    def show
      #authorize @gallery, :show?
    end

    def sort

    end

    def create
      @gallery = Gallery.new(gallery_params)
      render @gallery.save ? :show : :error
    end

    def update
      render @gallery.update(gallery_params) ? :gallery : :error
    end

    def destroy
      @gallery.destroy
      render json: {success: true}
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_gallery
      @gallery = Gallery.find(params[:id])
    end

    def set_project
      @project = Project.find(params[:project_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gallery_params
      params.require(:gallery).permit(:position, :name)
    end
  end
end
