class Gallery < ActiveRecord::Base
  belongs_to :project
  has_many :photos
  DEFAULT_NAME = 'Highlights'
  DEFAULT_POSITION = 0

  def self.default_params
    {
        name: DEFAULT_NAME,
        position: DEFAULT_POSITION
    }
  end
end
