class Project < ActiveRecord::Base
  belongs_to :user
  has_many :galleries

  validates :date, :name, presence: true

  after_create :create_default_gallery


  def create_default_gallery
    galleries.create(Gallery.default_params)
  end
end
