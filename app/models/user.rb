class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  include DeviseTokenAuth::Concerns::User

  mount_uploader :studio_logo, LogoUploader

  validates :studio_name, presence: true

  after_create :add_option_and_subscription

  has_many :projects
  has_one :option
  has_one :subscription

  private

  def add_option_and_subscription
    self.option = Option.create!
    self.subscription = Subscription.create!
  end
end
