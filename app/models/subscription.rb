class Subscription < ActiveRecord::Base
  belongs_to :user
  has_one :credit_card
  belongs_to :subscription_plan

  before_save :add_subscription_plan

  def self.calculate_storage_details(user)
    result = 0
    current_user = user
    current_user.projects.each do |project|
      project.galleries.each do |gallery|
        gallery.photos.each do |photo|
          result += photo.size
        end
      end
    end
    result
  end

  private

  def add_subscription_plan
    self.subscription_plan_id = 1
  end
end
