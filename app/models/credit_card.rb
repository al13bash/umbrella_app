class CreditCard < ActiveRecord::Base
  belongs_to :subscription
  validates :full_name, :address_line_1, :country, :city, :state,
            :postal, :card_number, :month, :year, :cvv, presence: true
  validates :card_number, :month, :year, :cvv, numericality: true
  validates :card_number, length: { is: 16 }
  validates :year, length: { is: 4 }
  validates :month, length: { is: 2 }
  validates :cvv, length: { is: 3 }
end
