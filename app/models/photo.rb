class Photo < ActiveRecord::Base
  belongs_to :gallery

  mount_uploader :photo, PhotoUploader

  before_save  :set_size

  def should_stamp?
    @logo = self.gallery.project.user.option.watermark
    self.gallery.project.watermark? && @logo.file
  end

  def stamp_watermark
    # TODO uncomment after misha's commit
    #self.photo.watermark!(@logo) if should_stamp?
  end

  def url
    photo.url
  end

  private

  def set_size
    self.size = self.photo.size
  end
end
