Rails.application.routes.draw do
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
  mount_devise_token_auth_for 'User', at: 'auth', controllers:
                              { registration: 'registrations' }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  concern :sortable do
    collection do
      post :sort
    end
  end

  namespace :api, defaults: { format: :json } do
    resources :projects do
      member do
        post :options, to: 'projects#set_options'
        get :options, to: 'projects#options'
      end
      resources :galleries, concerns: [:sortable] do
        resources :photos, concerns: [:sortable]
      end
    end

    get 'subscription' => 'settings/subscriptions#index'
    post 'subscription' => 'settings/subscriptions#update'

    get 'options' => 'settings/options#index'
    post 'options' => 'settings/options#update'

    get 'credit_card' => 'settings/credit_card#index'
    post 'credit_card' => 'settings/credit_card#update'

    scope :account do
      post :logo, to: 'settings/account#set_logo'
      get :logo, to: 'settings/account#logo'
    end

    scope :options do
      post :watermark, to: 'settings/options#set_watermark'
      get :watermark, to: 'settings/options#watermark'
    end
  end

  root 'main#index'
  get '*path' => 'main#index'
end
